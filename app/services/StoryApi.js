import { api } from './api';

export const getDetailStory = (idStory) => new Promise((resolve, reject) => {
    api.get(`stories?id=${idStory}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListChapter = (idStory, start, end) => new Promise((resolve, reject) => {
    api.get(`chapters/numbers?end=${end}&start=${start}&story_id=${idStory}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getContentChapter = (idChap, idStory) => new Promise((resolve, reject) => {
    api.get(`chapters/detail?number=${idChap}&story_id=${idStory}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryNew = (genre, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?genre=${genre}&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=create`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryUpdate = (genre, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?genre=${genre}&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=updated`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryFull = (genre, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?full=1&genre=${genre}&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=updated`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryViewMost = (genre, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?genre=${genre}&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=view_count`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryLikeMost = (genre, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?genre=${genre}&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=like_count`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const downloadStory = (end, idStory) => new Promise((resolve, reject) => {
    api.get(`chapters/numbers?end=${end}&start=1&story_id=${idStory}&type=all`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

