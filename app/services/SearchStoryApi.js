import { api } from './api';

export const getListStorySearch = (keyword, numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?keyword=${keyword}&limit=${numberOfItem}&&offset=${page* numberOfItem}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
}) 