import axios from 'axios';
import {
    Platform
} from 'react-native';
// import { getToken } from '../helpers/storage';

const BASE_API_URL = 'http://truyenyeuthich.com/api/v2/';

export const api = axios.create({
    baseURL: BASE_API_URL,
    timeout: 600000,
    headers: Platform.OS === 'ios' ?
        {
            'story_ios': 'zYmh2r',
            'client-id': 'simbo',
            'client-token': 'simbo',
            'client-platform': 'ios',
            'client-version': 16,
        }
        :
        {
            'story_android': 'zYmh2r',
            'client-id': 'simbo',
            'client-token': 'codaistory',
            'client-platform': 'android',
            'client-version': 16,
        },
});

// export function setToken(token) {
//   api.defaults.headers.common.Authorization = `Bearer ${token}`;
// }

// getToken().then((token) => {
//   setToken(token);
// });