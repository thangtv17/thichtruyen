import { api } from './api';

export const getContentTabDiscovery = () => new Promise((resolve, reject) => {
    api.get(`features`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListGenreStoryFull = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?full=1&limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=updated`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryNew = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=created`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryUpdate = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=update`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryViewMost = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=view_count`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryLikeMost = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/list?limit=${numberOfItem}&&offset=${page * numberOfItem}&sort=like_count`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryGroup = (idGroup) => new Promise((resolve, reject) => {
    api.get(`stories/group?id=${idGroup}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryTanDiO = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/group?id=59c8c99a0d83c12a1a0d85bf&limit=${numberOfItem}&&offset=${page * numberOfItem}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryHayP1 = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/group?id=59d1d650de9a66602b655720&limit=${numberOfItem}&&offset=${page * numberOfItem}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

export const getListStoryHayP2 = (numberOfItem, page) => new Promise((resolve, reject) => {
    api.get(`stories/group?id=5b57e77c4bf345e5c08d370e&limit=${numberOfItem}&&offset=${page * numberOfItem}`).then(response => {
        resolve(response.data.data);
    }).catch(error => {
        reject(error);
    });
})

