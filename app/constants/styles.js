import { StyleSheet, Dimensions, Platform } from 'react-native';
import colors from './colors';

import AppDimensions from "./dimensions"
import AppColors from "./colors"
export default styles = StyleSheet.create({
  styleTextToolbar: {
    fontSize: AppDimensions.SCREEN_WIDTH / 24,
    alignSelf: 'center'
  },
  containerView: {
    flex: 1,
    backgroundColor: colors.white,
  },
  titleHeader: {
    fontSize: AppDimensions.SCREEN_WIDTH / 24,
    fontWeight: "bold",
  },
  wrapperHeader: {
    backgroundColor: colors.colorPrimary,
    minHeight: 50,
    flexWrap: 'wrap',
    justifyContent: 'center',
    paddingHorizontal: 10
  },
  seperatorOfItemList: {
    width: AppDimensions.WINDOW_WIDTH,
    height: 1,
    backgroundColor: AppColors.colorBlack3,
    opacity: 0.2,
  }
});