// bg color
export const BACKGROUND_COLOR_WHITE = 1;
export const BACKGROUND_COLOR_BLACK = 2;
export const BACKGROUND_COLOR_YELLOW = 3;

// type read
export const TYPE_READ_VERTICAL = 1;
export const TYPE_READ_HORIZONTAL = 2;

// font family
export const FONT_FAMILY_SANS_SERIF = 1;
export const FONT_FAMILY_SERIF = 2;
export const FONT_FAMILY_BOOK_ERLY = 3;
export const FONT_FAMILY_HELVETICA = 4;

//
export const NUMBER_OF_ITEM_PER_PAGE = 36;