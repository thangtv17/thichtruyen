import Dimensions from "./dimensions";
import Icons from "./icons";
import Colors from "./colors";
import Styles from "./styles";
import Texts from "./text";
import * as Numbering from "./define";
export default {
  Dimensions,
  Icons,
  Colors,
  Styles,
  Texts,
  Numbering
}