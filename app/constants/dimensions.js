import {Dimensions} from "react-native";
export default dimensions = {
  WINDOW_WIDTH: Dimensions.get("window").width,
  WINDOW_HEIGHT: Dimensions.get("window").height,
  SCREEN_WIDTH: Dimensions.get("screen").width,
  SCREEN_HEIGHT: Dimensions.get("screen").height,
  NORMAL_PADDING_HORIZONTAL: 15,
  NORMAL_PADDING_VERTICAL: 15
}