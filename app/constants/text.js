export default texts = {
  genre: "Thể Loại",
  history: 'Lịch Sử',
  findStory:'Tìm Truyện',
  menu: "Menu",
  discovery: 'Khám Phá',
  newStory: "Mới đăng",
  viewMore: "Xem thêm",
  goodList: "Danh sách hay",
  newest: "Mới nhất",
  tabHistory: {
    viewedStory: "Truyện đã xem",
    favoriteStory: "Truyện bạn yêu thích",
    offlineStory: "Truyện đã tải - Đọc offline"
  },
  tabMenu: {
    settingUI: 'Cài đặt giao diện đọc truyện',
    settingBGColor: 'Chọn màu nền yêu thích của bạn',
    share: 'Chia sẻ ứng dụng',
    rate: 'Góp ý - Đánh giá ứng dụng'
  },
  newUpdated: "Mới cập nhật",
  noData: "Không có dữ liệu"
}
