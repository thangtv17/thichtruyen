import { StyleSheet } from "react-native";
import Constants from '../../constants';


export default styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingVertical: 10,
    width: Constants.Dimensions.WINDOW_WIDTH,
    // height: Constants.Dimensions.WINDOW_HEIGHT/5,
    alignItems: "center"
  },
  textContainer: {
    flex: 7,
    flexDirection: "column",
    marginLeft: 15
  },
  historyName: {
    color: Constants.Colors.colorPrimary,
    fontWeight: "bold",
    flex: 2,
  },
  subText: {
    fontSize: 13,
  },
  subTextWrapper: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  icon: {
    marginRight: 10
  },
  normalIcon: {
    width: 20,
    height: 20,
  },
  image: {

    height: 120,
    borderRadius: 2
  },
  circleIcon: {
    width: 35,
    height: 35,
    borderRadius: 35 / 2,
    borderColor: "#808080",
    borderWidth: 0.7,
    alignItems: "center",
    justifyContent: "center"
  },
  descriptionText: {
    fontSize: 16,
    marginLeft: 15,
    marginRight: 15,
    marginBottom: 10
  },
  n: {
    color: 'white'
  },
  styleButtonRead: {
    flex: 2, 
    alignItems: "center", 
    justifyContent: "center",
    backgroundColor: 'red'
  }
})