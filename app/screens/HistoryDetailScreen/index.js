import React from "react";
import { View, Text, TouchableOpacity, Image, StyleSheet, ScrollView } from "react-native";
import Constants from "../../constants";
import { MyStatusBar } from "../../components/MyStatusBar";
import Header from "../../components/Header";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { CustomHistoryItem } from "../../components/CustomHistoryItem";
import HTMLView from 'react-native-htmlview';
import { getDetailStory } from '../../services/StoryApi';
import styles from './styles';

export default class HistoryDetailScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      storyItem: props.navigation.getParam('storyItem', null),
      data: [],
      isLoading: true
    }
  }

  componentWillMount() {
    getDetailStory(this.state.storyItem._id.$oid).then(result => {
      this.setState({
        data: result,
        isLoading: false
      })
    }).catch(err => {
      console.log(err);
      this.setState({
        isLoading: false
      })
    });
  }

  _goBack = () => {
    this.props.navigation.goBack();
  }

  nClickShareButton = () => {

  }

  onClickLikeButton = () => {

  }

  onPressDownloadButton = () => {

  }

  onPressMenuButton = () => {

  }

  renderTopBody = () => (
    <Text style={Constants.Styles.titleHeader}>Thông tin truyện</Text>
  );

  renderInfoStory = (name, size, color, style, data) => {
    return (
      <View style={styles.subTextWrapper}>
        <Icon name={name} size={size} color={color} style={style} />
        <Text style={styles.subText}>{data.author}</Text>
      </View>
    );
  }

  render() {
    let { isLoading, data } = this.state;
    if (!isLoading) {
      return <View style={{ flex: 1 }}>
        <MyStatusBar backgroundColor={Constants.Colors.colorPrimaryDark} />
        <View style={Constants.Styles.wrapperHeader}>
          <Header
            left={this.renderTopLeft()}
            body={this.renderTopBody()}
          />
        </View>
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ paddingHorizontal: 10 }}>
            <View style={{ flex: 1 }}>
              <Text style={{
                fontWeight: "bold",
                fontSize: 15,
                marginTop: 10,
                marginBottom: 10
              }}>{data.title}</Text>
              <View
                style={styles.container}
              >
                <View style={{ justifyContent: "flex-start", flex: 2, }}>
                  <Image
                    style={styles.image}
                    source={{ uri: data.cover }} />
                </View>
                <View style={styles.textContainer}>

                  {this.renderInfoStory('pencil', 15, '#808080', styles.icon, data)}
                  {this.renderInfoStory('cloud-upload-outline', 15, '#808080', styles.icon, data)}
                  {this.renderInfoStory('menu', 15, '#808080', styles.icon, data)}

                  <View style={[styles.subTextWrapper, { marginVertical: 10 }]}>
                    <TouchableOpacity style={styles.circleIcon} onPress={this.onClickLikeButton}>
                      <Icon name="heart" size={18} color={Constants.Colors.colorPrimary} />
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.circleIcon, { marginLeft: 10 }]}
                      onPress={this.onClickShareButton}>
                      <Icon name="share" size={18} color={Constants.Colors.colorPrimary} />
                    </TouchableOpacity>
                  </View>

                </View>
              </View>
              <HTMLView stylesheet={styles} value={
                data.desc
                  .replace(/<br><br>/gi, '<n>123</n>')
                  .replace(/<br>/gi, '')}
              />

            </View>
          </ScrollView>
          <View style={{ width: Constants.Dimensions.WINDOW_WIDTH, height: 50, alignItems: "center", flexDirection: "row", position: "relative" }}>
            <TouchableOpacity style={{ flex: 1, alignItems: "center" }} onPress={this.onPressDownloadButton}>
              <Icon name="cloud-download" size={20} color={Constants.Colors.colorPrimary} />
              <Text style={{ color: Constants.Colors.colorPrimary }}>Tải về</Text>
            </TouchableOpacity>
            <View style={styles.styleButtonRead}>
              <TouchableOpacity>
                <Text style={{ fontWeight: "bold", color: Constants.Colors.colorPrimary }}>Đọc truyện</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={{ flex: 1, alignItems: "center" }} onPress={this.onPressMenuButton}>
              <Icon name="menu" size={20} color={Constants.Colors.colorPrimary} />
              <Text style={{ color: Constants.Colors.colorPrimary }}>Mục lục</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    } else {
      return null;
    }
  }

  renderTopLeft = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
        <TouchableOpacity style={{ width: 34, height: 34, justifyContent: 'center', marginLeft: 5 }}
          onPress={this._goBack}>
          <Image source={Constants.Icons.IC_BACK}
            style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: 'white' }} />
        </TouchableOpacity>
      </View>
    );
  };
}

