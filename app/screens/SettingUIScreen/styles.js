import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  styleHeader: {
    minHeight: 50,
    flexWrap: 'wrap',
    justifyContent: 'center'
  },
  styleTopHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  styleButtonTopLeft: {
    width: 34,
    height: 34,
    justifyContent: 'center',
    paddingLeft: 15
  },
  styleImageTopLeft: {
    width: 15,
    height: 15,
    resizeMode: 'contain',
    tintColor: 'black'
  },
  styleText: {
    fontSize: 14,
    marginBottom: 15,
    color: 'black'
  },
  styleContainerScroll: {
    padding: 15
  },
  styleView: {
    marginBottom: 10
  }
});