import React from 'react';
import {
  Image,
  Platform,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { MyStatusBar } from '../../components/MyStatusBar';
import styles from './styles';
import Constants from "../../constants";
import { connect } from 'react-redux';
import Header from '../../components/Header';
import { ScrollView } from 'react-native-gesture-handler';
import { CustomSettingUIItem } from '../../components/CustomerSettingUIItem';
import { FlatGrid } from 'react-native-super-grid';
import { listBGColor, styleRead, fontFamily, distanceRow, fontTextSize } from '../../data';
import { changeBackgroundColor, changeTypeRead, changeFontSize, changeFontFamily, changeDistanceRow } from '../../redux/actions/settingUI';
class HomeScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this._goBack = this._goBack.bind(this);
    this._onChangeBackgroundColor = this._onChangeBackgroundColor.bind(this);
    this._onChangeTypeRead = this._onChangeTypeRead.bind(this);
    this._onChangeFontSize = this._onChangeFontSize.bind(this);
    this._onChangeFontFamily = this._onChangeFontFamily.bind(this);
    this._onChangeDistanceRow = this._onChangeDistanceRow.bind(this);

    this.state = {
      backgroundColor: props.backgroundColor,
      typeRead: props.typeRead,
      fontSize: props.fontSize,
      fontFamily: props.fontFamily,
      distanceRow: props.distanceRow
    };
  }

  componentWillReceiveProps = (nextProps) => {
    if (this.props.backgroundColor !== nextProps.backgroundColor) {
      this.setState({
        backgroundColor: nextProps.backgroundColor,
      })
    }
    if (this.props.typeRead !== nextProps.typeRead) {
      this.setState({
        typeRead: nextProps.typeRead,
      })
    }
    if (this.props.fontSize !== nextProps.fontSize) {
      this.setState({
        fontSize: nextProps.fontSize,
      })
    }
    if (this.props.fontFamily !== nextProps.fontFamily) {
      this.setState({
        fontFamily: nextProps.fontFamily,
      })
    }
    if (this.props.distanceRow !== nextProps.distanceRow) {
      this.setState({
        distanceRow: nextProps.distanceRow,
      })
    }
  }

  _goBack = () => {
    this.props.navigation.goBack();
  }

  _onChangeBackgroundColor = (backgroundColor) => () => {
    this.props.changeBackgroundColor(backgroundColor);
  }

  _onChangeTypeRead = (typeRead) => () => {
    this.props.changeTypeRead(typeRead);
  }

  _onChangeFontSize = (fontSize) => () => {
    this.props.changeFontSize(fontSize);
  }

  _onChangeFontFamily = (fontFamily) => () => {
    this.props.changeFontFamily(fontFamily);
  }

  _onChangeDistanceRow = (distanceRow) => () => {
    this.props.changeDistanceRow(distanceRow);
  }

  renderHeader() {
    return (
      <View style={[{ backgroundColor: Constants.Colors.white }, styles.styleHeader]}>
        <Header
          left={this.renderTopLeft()}
          body={this.renderTopBody()}
        />
      </View>
    );
  }

  renderTopLeft() {
    return (
      <View style={styles.styleTopHeader}>
        <TouchableOpacity style={styles.styleButtonTopLeft} onPress={this._goBack}>
          <Image source={Constants.Icons.IC_CANCEL} style={styles.styleImageTopLeft} />
        </TouchableOpacity>
      </View >
    );
  }

  renderTopBody() {
    return (
      <View style={styles.styleTopHeader}>
        <Text style={Constants.Styles.styleTextToolbar}>
          Thiết lập giao diện
        </Text>
      </View>
    );
  }

  render() {
    let { backgroundColor, typeRead, fontSize, fontFamily } = this.state;
    return (
      <View style={{ flex: 1 }}>
        <MyStatusBar backgroundColor={Constants.Colors.colorPrimaryDark} />
        {this.renderHeader()}
        <ScrollView contentContainerStyle={styles.styleContainerScroll}>
          <View style={styles.styleView}>
            <Text style={styles.styleText}>Màu nền</Text>
            <FlatList
              data={listBGColor}
              renderItem={({ item, index }) =>
                <CustomSettingUIItem
                  text={item.name}
                  idChoose={backgroundColor === item.id}
                  onClick={this._onChangeBackgroundColor(item.id)}
                />
              }
              keyExtractor={item => item.id}
              numColumns={3}
              extraData={this.state}
            />
          </View>
          <View style={styles.styleView}>
            <Text style={styles.styleText}>Kiểu đọc</Text>
            <FlatList
              data={styleRead}
              renderItem={({ item, index }) =>
                <CustomSettingUIItem
                  text={item.name}
                  idChoose={typeRead === item.id}
                  onClick={this._onChangeTypeRead(item.id)}
                />
              }
              keyExtractor={item => item.id}
              numColumns={2}
              extraData={this.state}
            />
          </View>
          <View style={styles.styleView}>
            <Text style={styles.styleText}>Cỡ chữ</Text>
            <FlatGrid
              spacing={0}
              itemDimension={55}
              items={fontTextSize}
              renderItem={({ item, index }) =>
                <CustomSettingUIItem
                  text={item.id}
                  idChoose={fontSize === item.id}
                  onClick={this._onChangeFontSize(item.id)}
                />
              }
            />
          </View>
          <View style={styles.styleView}>
            <Text style={styles.styleText}>Kiểu chữ</Text>
            <FlatList
              data={fontFamily}
              renderItem={({ item, index }) =>
                <CustomSettingUIItem
                  text={item.name}
                  idChoose={fontFamily === item.id}
                  onClick={this._onChangeFontFamily(item.id)}
                />
              }
              keyExtractor={item => item.id}
              numColumns={4}
            />
          </View>
          <View style={styles.styleView}>
            <Text style={styles.styleText}>Khoảng cách giữa các dòng</Text>
            <FlatGrid
              spacing={0}
              itemDimension={55}
              items={distanceRow}
              renderItem={({ item, index }) =>
                <CustomSettingUIItem
                  text={item.id}
                  idChoose={this.state.distanceRow === item.id}
                  onClick={this._onChangeDistanceRow(item.id)}
                />
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { backgroundColor, typeRead, fontSize, fontFamily, distanceRow } = state.settingUI;
  return {
    backgroundColor,
    typeRead,
    fontSize,
    fontFamily,
    distanceRow
  };
}

const mapDispatchToProps = {
  changeBackgroundColor, changeTypeRead, changeFontSize, changeFontFamily, changeDistanceRow
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);

