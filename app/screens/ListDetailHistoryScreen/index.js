import React from "react";
import {View, TouchableOpacity, Image, Text} from "react-native";
import {MyStatusBar} from "../../components/MyStatusBar";
import Constants from "../../constants";
import Header from "../../components/Header";
import HistoryListComponent from "../../components/HistoryListComponent";

class ListDetailHistoryScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      title: props.navigation.getParam("title", ""),
      previousScreen: props.navigation.getParam("previousScreen", ""),
      previousScreenName: props.navigation.getParam("previousScreenName", ""),
    }
  }


  render() {
    return (
      <View style={{flex: 1}}>
        <MyStatusBar backgroundColor={Constants.Colors.colorPrimaryDark}/>
        <View style={Constants.Styles.wrapperHeader}>
          <Header
            left={this.renderTopLeft()}
            body={this.renderTopBody()}
            right={this.renderTopRight()}
          />
        </View>
        <HistoryListComponent
          title={this.state.title}
          previousScreen={this.state.previousScreen}
          previousScreenName={this.state.previousScreenName}
          onClickHistoryItem={this.onClickHistoryItem}
          navigation={this.props.navigation}
        />
      </View>
    )
  }

  renderTopLeft = () => {
    return (
      <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
        <TouchableOpacity style={{width: 34, height: 34, justifyContent: 'center', marginLeft: 5}}
                          onPress={this._goBack}>
          <Image source={Constants.Icons.IC_BACK}
                 style={{width: 20, height: 20, resizeMode: 'contain', tintColor: 'white'}}/>
        </TouchableOpacity>
        <Text style={Constants.Styles.styleTextToolbar}>{this.state.previousScreenName}</Text>
      </View>
    );
  };
  renderTopRight = () => {

  };
  renderTopBody = () => (
    <Text style={Constants.Styles.titleHeader}>{this.state.title}</Text>
  );
  onClickHistoryItem = (index) => {
    alert("click item " + index);
  }
  _goBack = () => {
    this.props.navigation.goBack();
  }
}

export default ListDetailHistoryScreen;