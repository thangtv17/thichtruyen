import React from 'react';
import { Image, Text, TouchableOpacity, View, TouchableWithoutFeedback } from 'react-native';
import { TabBar, TabView } from "react-native-tab-view";
import ListDetailHistoryScreen from '../ListDetailHistoryScreen'
import Constants from "../../constants";
import HistoryListComponent from "../../components/HistoryListComponent";
import { MyStatusBar } from "../../components/MyStatusBar";
import Header from "../../components/Header";
import styles from './styles';
import Spinner from 'react-native-loading-spinner-overlay';

// This is our placeholder component for the tabs
// This will be rendered when a tab isn't loaded yet
// You could also customize it to render different content depending on the route
const LazyPlaceholder = ({ route }) => (
  <View style={styles.scene}>
    <Text>Loading {route.title}…</Text>
  </View>
);

class GenreScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      routes: [
        { key: 'newUpload', title: 'Mới đăng' },
        { key: 'newUpdate', title: 'Mới cập nhật' },
        { key: 'full', title: 'FULL' },
        { key: 'viewMost', title: 'Xem nhiều' },
        { key: 'likeMost', title: 'Yêu thích' }
      ],
      loaded: ['newUpload'],
      genre: props.navigation.getParam('genre', ''),
      title: props.navigation.getParam('genre', ''),
      previousScreen: props.navigation.getParam('previousScreen', ''),
      previousScreenName: props.navigation.getParam('previousScreenName', '')
    }
  }

  _goBack = () => {
    this.props.navigation.goBack();
  }

  _handleIndexChange = index => {
    this.setState(state => {
      const { key } = state.routes[index];

      return {
        index,
        // If the route isn't loaded already, add it's key to the loaded list
        // This way routes will be loaded as we navigate to them
        loaded: state.loaded.includes(key)
          ? state.loaded
          : [...state.loaded, key],
      };
    });
  }

  _renderScene = ({ route }) => {
    if (
      this.state.routes.indexOf(route) !== this.state.index &&
      this.state.routes.indexOf(route) !== (this.state.index + 1) &&
      !this.state.loaded.includes(route.key)
    ) {
      // If the route is not focused and not loaded, render a placeholder
      return <LazyPlaceholder route={route} />;
    }

    return <HistoryListComponent
      title={this.state.title}
      previousScreen={this.state.previousScreen}
      previousScreenName={this.state.previousScreenName}
      onClickHistoryItem={this.onClickHistoryItem}
      sortType={route.key}
      navigation={this.props.navigation}
    />
  }

  _renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled
      indicatorStyle={styles.indicator}
      style={styles.tabbar}
      tabStyle={styles.tab}
      labelStyle={styles.label}
      renderLabel={({ route }) => (
        <Text style={styles.textTabbarStyle}>
          {route.title}
        </Text>
      )}
    />
  );

  renderTopLeft = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', flex: 1 }}>
        <TouchableOpacity style={{ width: 34, height: 34, justifyContent: 'center', marginLeft: 5 }}
          onPress={this._goBack}>
          <Image source={Constants.Icons.IC_BACK}
            style={{ width: 20, height: 20, resizeMode: 'contain', tintColor: 'white' }} />
        </TouchableOpacity>
        <Text style={Constants.Styles.styleTextToolbar}>{this.state.previousScreenName}</Text>
      </View>
    );
  };

  renderTopRight = () => {

  };
  renderTopBody = () => (
    <Text style={Constants.Styles.titleHeader}>{this.state.title}</Text>
  );

  render() {
    return <View style={{ flex: 1 }}>
      <MyStatusBar backgroundColor={Constants.Colors.colorPrimaryDark} />
      <View style={Constants.Styles.wrapperHeader}>
        <Header
          left={this.renderTopLeft()}
          body={this.renderTopBody()}
          right={this.renderTopRight()}
        />
      </View>
      <TabView
        navigationState={this.state}
        renderScene={this._renderScene}
        renderTabBar={this._renderTabBar}
        swipeEnabled
        onIndexChange={this._handleIndexChange}
        tabBarPosition='top'
      />
    </View>
  }
}

export default GenreScreen;