import { StyleSheet } from "react-native";
import Constants from '../../constants';

export default styles = StyleSheet.create({
  tabbar: {
    backgroundColor: 'white'
  },
  iconTab: {
    resizeMode: 'contain',
    height: 22,
    width: 22,
    marginBottom: 5
  },
  indicator: {
    backgroundColor: Constants.Colors.colorBlack3,
    height: 1,
    opacity: 0.2,
    elevation: 5,
    shadowColor: Constants.Colors.colorBlack3,
    shadowOffset: { width: 0, height: 5 },
  },
  tab: {
    width: Constants.Dimensions.WINDOW_WIDTH * 0.3,
  },
  label: {
    fontWeight: '400',
  },
  textTabbarStyle: {
    color: Constants.Colors.colorBlack3,
    margin: 4
  },
  spinnerTextStyle: {
    color: Constants.Colors.white,
    fontSize: 10
  },
  scene: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});