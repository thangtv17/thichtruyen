import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import styles from './style';
import Constants from '../../../constants';
import { CustomHistoryItem } from '../../../components/CustomHistoryItem'


class MenuTab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      sections: [
        {
          icon: Constants.Icons.IC_SETTING_UI,
          text: Constants.Texts.tabMenu.settingUI,
        }, {
          icon: Constants.Icons.IC_SETTING_BG_COLOR,
          text: Constants.Texts.tabMenu.settingBGColor,
        }, {
          icon: Constants.Icons.IC_SHARE,
          text: Constants.Texts.tabMenu.share,
        }, {
          icon: Constants.Icons.IC_RATE,
          text: Constants.Texts.tabMenu.rate,
        }
      ]
    };
  }

  onClickSection = (index) => {
    switch (index) {
      case 0:
        this.props.navigation.navigate('SettingUI');
        break;
      case 1:
        alert("click item 1");
        break;
      case 2:
        alert("click item 2");
        break;
    }
  }

  render() {
    return (
      <ScrollView
        style={
          [
            Constants.Styles.containerView,
            {
              paddingLeft: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL,
              paddingRight: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL,
            }
          ]
        }
        contentContainerStyle={{ flex: 1, alignItems: 'center' }}>
        {
          this.state.sections.map((item, index) => (
            <CustomHistoryItem
              key={index}
              icon={item.icon}
              text={item.text}
              onClick={() => this.onClickSection(index)}
            />
          ))
        }
      </ScrollView>
    );
  }
}

function mapStateToProps(state) {
  return {
  };
}

const mapDispatchToProps = {

};


export default connect(mapStateToProps, mapDispatchToProps)(MenuTab);