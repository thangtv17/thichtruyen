import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import {connect} from 'react-redux';
import styles from './style';
import {CustomGenreItem} from '../../../components/CustomGenreItem';
import * as StoryApi from '../../../services/StoryApi'
import Constants from "../../../constants";

class GenreTab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      listData: [
        "Ngôn Tình", "Ngược",
        "Đam Mỹ", "Hài Hước",
        "Đô Thị", "Xuyên Không",
        "Truyện teen", "Kiếm Hiệp",
        "Tiên Hiệp", "Sắc",
        "Sủng", "Mạt Thế",
        "Bách Hợp", "Đông Phương",
        "Cung Đấu"
      ]
    };
    this.onPressItem = this.onPressItem.bind(this);
  }

  componentDidMount() {
    if (this.state.listData && this.state.listData.length % 2 === 1) {
      this.setState({listData: [...this.state.listData, ""]});
    }
  }

  render() {
    return (
      <FlatList
        style={{flex: 1}}
        renderItem={
          ({item, index}) => (
            <CustomGenreItem
              text={item}
              onPressItem={this.onPressItem(index)}
            />
          )}
        data={this.state.listData}
        keyExtractor={(item, index) => index.toString()}
        numColumns={2}/>
    );
  }

  onPressItem = (index) => () => {
    this.props.navigation.navigate('Genre', {
      previousScreen: "GenreTab",
      previousScreenName: Constants.Texts.genre,
      title: Constants.Texts.newUpdated,
      genre: this.state.listData[index]
    });
  }
}

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};


export default connect(mapStateToProps, mapDispatchToProps)(GenreTab);