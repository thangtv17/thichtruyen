import React from 'react';
import {
  Image,
  Platform,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Animated
} from 'react-native';
import GenreTab from './GenreTab';
import DiscoveryTab from './DiscoveryTab';
import FindStoryTab from './FindStoryTab';
import HistoryTab from './HistoryTab';
import MenuTab from './MenuTab';

import { MyStatusBar } from '../../components/MyStatusBar';
import { TabView } from 'react-native-tab-view';
import { connect } from "react-redux";

import Titlebar from '../../components/TitleBar';
import styles from "./styles";

import Constants from "../../constants"
class HomeScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      index: 1,
      routes: [
        { key: 'Genre', title: Constants.Texts.genre },
        { key: 'Discovery', title: Constants.Texts.discovery },
        { key: 'FindStory', title: Constants.Texts.findStory },
        { key: 'History', title: Constants.Texts.history },
        { key: 'Menu', title: Constants.Texts.menu },
      ],
      title: Constants.Texts.genre
    };

  }

  _renderTabBar = props => {
    const inputRange = props.navigationState.routes.map((x, i) => i);
    return (
      <View style={styles.tabBar}>
        {props.navigationState.routes.map((route, i) => {
          const color = props.position.interpolate({
            inputRange,
            outputRange: inputRange.map(
              inputIndex => (inputIndex === i ? Constants.Colors.colorPrimaryDark : 'gray')
            ),
          });
          let tintColor = i === this.state.index ? Constants.Colors.colorPrimaryDark : 'gray';
          let iconName = 'ios-analytics-outline';
          let n = i % 5;
          if (n === 0) {
            iconName = Constants.Icons.IC_GENRE
          }
          if (n === 1) {
            iconName = Constants.Icons.IC_DISCOVERY
          }
          if (n === 2) {
            iconName = Constants.Icons.IC_FIND
          }
          if (n === 3) {
            iconName = Constants.Icons.IC_HISTORY
          }
          if (n === 4) {
            iconName = Constants.Icons.IC_MENU
          }
          return (
            <TouchableOpacity
              key={i}
              style={styles.tabItem}
              activeOpacity={0.1}
              onPress={() => this.setState({ index: i , title: this.state.routes[i].title})}
            >
              <Image source={iconName} style={[styles.iconTab, { tintColor: tintColor }]} />
              <Animated.Text style={{ color, fontSize: 12 }}
              >
                {route.title}
              </Animated.Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  _renderScene = ({ route }) => {
    switch (route.key) {
      case 'Genre':
        return (
          <GenreTab
            navigation={this.props.navigation}
          />
        );
      case 'Discovery':
        return (
          <DiscoveryTab
            navigation={this.props.navigation}
          />
        );
      case 'FindStory':
        return (
          <FindStoryTab
            navigation={this.props.navigation}
          />
        );
      case 'History':
        return (
          <HistoryTab
            navigation={this.props.navigation}
          />
        );
      case 'Menu':
        return (
          <MenuTab
            navigation={this.props.navigation}
            toggleBottomTab={() => {
              LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
              this.setState(prevState => ({
                expandedBottomTab: !prevState.expandedBottomTab
              }))
            }}
            valueSelectBottom={this.state.valueSelectBottom}
            isActived={this.state.index === 3}
          />
        );
      default:
        return null;
    }
  };

  render() {
    return (
      <View style={{ flex: 1 }}>
        <MyStatusBar backgroundColor={Constants.Colors.colorPrimaryDark} />
        <Titlebar title={this.state.title}/>
        <TabView
          navigationState={this.state}
          renderScene={this._renderScene}
          swipeEnabled
          onIndexChange={index => {
            this.setState({ index, title: this.state.routes[index].title })
          }}
          tabBarPosition='bottom'
          renderTabBar={this._renderTabBar}
        />
      </View>
    );
  }

}


function mapStateToProps(state) {
  return {

  };
}
export default connect(mapStateToProps, null)(HomeScreen);
