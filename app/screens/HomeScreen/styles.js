import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
  },
  tabItem: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
    height: 60
  },
  tabBar: {
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  iconTab: {
    resizeMode: 'contain',
    height: 18,
    width: 18,
    marginBottom: 5
  },
  labelStyle: {

  },
  typeQuestionBox: {
    height: 25,
    width: 25,
    borderRadius: 5,
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#fff',
    alignItems: "center",
    marginLeft: 5,
    marginTop: 2
  }
});