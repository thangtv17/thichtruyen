import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import styles from './style';
import Constants from '../../../constants';
import SectionDiscoveryItem from "../../../components/SectionDiscoveryItem"
import { CustomHistoryItem } from "../../../components/CustomHistoryItem"
import { getContentTabDiscovery } from '../../../services/DiscoveryApi';

class DiscoveryTab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.onClickNewStorySection = this.onClickNewStorySection.bind(this);
    this.onClickNewStory = this.onClickNewStory.bind(this);
    this.onClickGoodList = this.onClickGoodList.bind(this);

    this.isLoading = true;

    this.state = {
      goodList: [
        {
          "_id": {
            "$oid": "1"
          },
          "title": "Truyện Full - Hoàn thành"
        },
        {
          "_id": {
            "$oid": "2"
          },
          "title": "Top truyện được xem nhiều nhất"
        },
        {
          "_id": {
            "$oid": "3"
          },
          "title": "Top truyện được yêu thích nhất"
        },
      ]
    };
  }

  componentDidMount() {
  }

  componentWillMount() {
    getContentTabDiscovery().then(response => {
      this.setState({
        newPost: response.new.slice(1, 10),
        newUpdate: response.updated.slice(1, 10),
        goodList: [...this.state.goodList, ...response.story_group],
        fullNewPost: response.new,
        fullNewUpdate: response.updated,
      }, () => {
        this.isLoading = false;
      })
    })
  }

  onClickNewStory = (index) => {
    alert("onClickNewStory " + index);
  }

  onClickNewStorySection = () => {
    this.props.navigation.navigate('ListDetailHistoryScreen', {
      previousScreen: "DiscoveryTab",
      previousScreenName: Constants.Texts.discovery,
      title: Constants.Texts.newest
    });
  }

  onClickNewUpdate = (index) => {
    alert("onClickNewUpdate " + index);
  }

  onClickNewUpdateSection = () => {
    this.props.navigation.navigate('ListDetailHistoryScreen', {
      previousScreen: "DiscoveryTab",
      previousScreenName: Constants.Texts.discovery,
      title: Constants.Texts.newUpdated
    });
  }

  onClickGoodList = (item) => () => {
    this.props.navigation.navigate('ListStory', {
      previousScreen: "DiscoveryTab",
      previousScreenName: Constants.Texts.discovery,
      item: item
    });
  }

  render() {
    if (!this.isLoading) {
      return null;
    } else {
      return (
        <ScrollView
          style={Constants.Styles.containerView}
        // contentContainerStyle={{flex: 1, alignItems: 'center'}}
        >
          <SectionDiscoveryItem
            listData={this.state.newPost}
            onItemClick={this.onClickNewStory}
            onSectionClick={this.onClickNewStorySection}
            title={Constants.Texts.newStory}
          />
          <SectionDiscoveryItem
            listData={this.state.newUpdate}
            onItemClick={this.onClickNewUpdate}
            onSectionClick={this.onClickNewUpdateSection}
            title={Constants.Texts.newUpdated}
          />
          <View style={{ flexDirection: "column", padding: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL }}>
            <View style={{ alignItems: "flex-start" }}>
              <Text style={{ fontWeight: "bold" }}>{Constants.Texts.goodList}</Text>
            </View>
            {
              this.state.goodList.map((item, index) => (
                (<CustomHistoryItem
                  key={index}
                  text={item.title}
                  onClick={this.onClickGoodList(item)}
                />
                )
              ))
            }
          </View>
        </ScrollView>
      );
    }
  }
}

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};


export default connect(mapStateToProps, mapDispatchToProps)(DiscoveryTab);