import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  TouchableWithoutFeedback,
  Image,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import styles from './style';
import Constants from "../../../constants";
import HistoryListComponent from "../../../components/HistoryListComponent";
import SearchBar from "../../../components/SearchBar";

let DismissKeyboard = require('dismissKeyboard');

class FindStoryTab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      searchText: "",
      data: []
    };

  }
  
  updateSearch = searchText => {
    this.setState({ searchText });
  };
  
  render() {
    const { searchText, data } = this.state;
    return (
      <TouchableWithoutFeedback onPress={() => { DismissKeyboard() }}>
        <View style={{ flex: 1, alignItems: 'center', flexDirection: "column" }}>
          <SearchBar
            onTextChange={this.updateSearch}
            placeHolder={"Nhập tên truyện, thể loại, tác giả cần tìm"}
          />
          {searchText !== "" ?
            <HistoryListComponent
              style={{ flex: 1 }}
              title={""}
              previousScreen={"FindStoryTab"}
              previousScreenName={Constants.Texts.findStory}
              onClickHistoryItem={this.onClickHistoryItem}
              keyword={searchText}
            /> :
            <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
              <Text style={styles.centerText}>
                Tìm truyện ‘Anh có thích nước mỹ không’
              </Text>
              <Text style={styles.centerText}>
                Bạn có thể nhập không dấu: ‘anh co thich’ hoặc chỉ 2 từ ‘nuoc my’
              {"\n"}{"\n"}
                Hoặc tìm Theo tác giả: ’tan di o’
            </Text>
            </View>
          }
        </View>
      </TouchableWithoutFeedback>
    );
  }
  onClickHistoryItem = () => {

  }
}

function mapStateToProps(state) {
  return {
  };
}

const mapDispatchToProps = {

};


export default connect(mapStateToProps, mapDispatchToProps)(FindStoryTab);