import {StyleSheet} from 'react-native';
import Constants from '../../../constants';


export default styles = StyleSheet.create({
  centerText: {
    textAlign: "center",
    color: "black",
    marginLeft: 10,
    marginRight: 10,
    fontSize: 15,
  }
});