import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Text,
  ScrollView,
  Image,
  Dimensions
} from 'react-native';
import { connect } from 'react-redux';
import styles from './style';
import Constants from '../../../constants';
import { CustomHistoryItem } from '../../../components/CustomHistoryItem'

class HistoryTab extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      sections: [
        {
          icon: Constants.Icons.IC_CLOCK,
          text: Constants.Texts.tabHistory.viewedStory,
        }, {
          icon: Constants.Icons.IC_HEART,
          text: Constants.Texts.tabHistory.favoriteStory,
        }, {
          icon: Constants.Icons.IC_DOWNLOAD,
          text: Constants.Texts.tabHistory.offlineStory,
        }
      ]
    };

  }

  render() {
    return (
      <ScrollView
        style={
          [
            Constants.Styles.containerView,
            {
              paddingLeft: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL,
              paddingRight: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL,
            }
          ]
        }
        contentContainerStyle={{ flex: 1, alignItems: 'center' }}>
        {
          this.state.sections.map((item, index) => (
            <CustomHistoryItem
              key={index}
              icon={item.icon}
              text={item.text}
              onClick={() => this.onClickSection(index)}
            />
          ))
        }
      </ScrollView>
    );
  }

  onClickSection = (index) => {
    switch (index) {
      case 0:
        alert("click item 0");
        break;
      case 1:
        alert("click item 1");
        break;
      case 2:
        alert("click item 2");
        break;
    }
  }
}

function mapStateToProps(state) {
  return {};
}

const mapDispatchToProps = {};


export default connect(mapStateToProps, mapDispatchToProps)(HistoryTab);