import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
  icon: {
    width: 20,
    height: 20,
  },
  styleText: {
    fontSize: 14,
    color: 'white',
  }
});