import * as DateTimeUtils from "./DateTimeUtils";
import * as ImageUtils from "./ImageUtils";
import * as StorageUtils from "./StorageUtils";

export default {
  DateTimeUtils,
  ImageUtils,
  StorageUtils
}