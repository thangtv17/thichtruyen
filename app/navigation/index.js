import React from 'react';
import { createStackNavigator, addNavigationHelpers, createAppContainer } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';
import ListDetailHistoryScreen from '../screens/ListDetailHistoryScreen';
import ListStoryScreen from '../screens/ListStoryScreen';
import SettingUIScreen from '../screens/SettingUIScreen';
import GenreScreen from '../screens/GenreScreen';
import HistoryDetailScreen from '../screens/HistoryDetailScreen';
import SplashScreen from '../screens/SplashScreen';
import ContentStoryScreen from '../screens/ContentStoryScreen';

console.disableYellowBox = true;

const AppNavigator = createStackNavigator({
  Splash: { screen: ContentStoryScreen },
  Home: { screen: HomeScreen },
  ListDetailHistoryScreen: { screen: ListDetailHistoryScreen },
  ListStory: { screen: ListStoryScreen },
  SettingUI: { screen: SettingUIScreen },
  Genre: { screen: GenreScreen },
  HistoryDetail: {screen: HistoryDetailScreen},
  ContentStory: { screen: ContentStoryScreen },
},
  {
    headerMode: 'none',
    mode: 'modal',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default AppContainer;
