import {StyleSheet, Dimensions} from "react-native";
import Constants from "../../constants";
export default styles= StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    height: Constants.Dimensions.WINDOW_HEIGHT/12,
  },
  subContainer: {

  },
  text:{
    flex: 7
  },
  icon:{
    height: 15,
    width: 15,
    tintColor: Constants.Colors.colorPrimary,
  },
  iconWrapper: {
    flex: 1,
    alignItems: "flex-start"
  },
  rightArrowWrapper: {
    flex: 1,
    alignItems: "flex-end",
  },
  rightArrow:{
    height: 15,
    width: 15,
  }
})
