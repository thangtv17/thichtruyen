import { StyleSheet, Dimensions } from "react-native";
import Constant from "../../constants";
import DimensionConstant from "../../constants/dimensions";

export default styles = StyleSheet.create({
  styleTextTitleBar: {
    fontWeight: 'bold'
  },
  titleLayout: {
    justifyContent: "center",
    alignItems: "center"
  },
  subContainer: {
    flexDirection: 'row',
    flex: 1,
    // padding: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    width: DimensionConstant.WINDOW_WIDTH,
    height: DimensionConstant.WINDOW_HEIGHT / 11,
    flexDirection: "column",
  },
  bottomLine: {
    width: DimensionConstant.SCREEN_WIDTH,
    height: 1,
    backgroundColor: Constant.Colors.colorBlack3,
    opacity: 0.2,
    shadowColor: Constant.Colors.colorBlack3,
    shadowOffset: { width: 0, height: 5 },
  }
})