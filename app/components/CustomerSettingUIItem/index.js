import React from "react";
import { TouchableOpacity, View, Text } from "react-native";
import styles from "./styles"
import Constants from '../../constants';

export const CustomSettingUIItem = ({ text, onClick, idChoose }) => (
  <TouchableOpacity
    style={[styles.container, { backgroundColor: idChoose ? Constants.Colors.colorBlue : Constants.Colors.colorBlack5 }]}
    onPress={onClick}
  >
    <Text style={[styles.text, { color: idChoose ? Constants.Colors.white : Constants.Colors.colorBlack2 }]}>{text}</Text>
  </TouchableOpacity>
);