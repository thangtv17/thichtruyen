import {StyleSheet} from "react-native";
import Constants from "../../constants";

export default styles = StyleSheet.create({
  container: {
    flexDirection: "column", height: 270,
    width: Constants.Dimensions.WINDOW_WIDTH, padding: Constants.Dimensions.NORMAL_PADDING_HORIZONTAL,
  }
})