import PropTypes from 'prop-types';
import React from 'react';
import {
  View,
} from 'react-native';
import DimensionConstant from "../../constants/dimensions";
import Constant from "../../constants";

export default class Header extends React.PureComponent {
  static propTypes = {
    body: PropTypes.element,
    bodyStyle: PropTypes.object,
    containerStyle: PropTypes.object,
    left: PropTypes.element,
    leftStyle: PropTypes.object,
    right: PropTypes.element,
    rightStyle: PropTypes.object,
    style: PropTypes.object,
  };

  setNativeProps(nativeProps) {

  }

  renderLeft() {
    return this.props.left;
  }

  renderRight() {
    return this.props.right;
  }

  renderBody() {
    return this.props.body;
  }

  render() {
    return (
      <View>
        <View style={{ ...styles.rootContainer, ...this.props.style }}>
          <View style={{ ...styles.leftContainer, ...this.props.leftStyle }}>
            {this.renderLeft()}
          </View>
          <View style={{ ...styles.bodyContainer, ...this.props.bodyStyle }}>
            {this.renderBody()}
          </View>
          <View style={{ ...styles.rightContainer, ...this.props.rightStyle }}>
            {this.renderRight()}
          </View>
        </View>
        <View style={styles.bottomLine} elevation={5} />
      </View>
    );
  }
}

const styles = {
  rootContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: DimensionConstant.SCREEN_HEIGHT / 11,
  },

  leftContainer: {
    flex: 0,
  },

  bodyContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  rightContainer: {
    flex: 0,
  },

  bottomLine: {
    width: DimensionConstant.SCREEN_WIDTH,
    height: 1,
    backgroundColor: Constant.Colors.colorBlack3,
    opacity: 0.2,
    shadowColor: Constant.Colors.colorBlack3,
    shadowOffset: { width: 0, height: 5 },
  }
};
